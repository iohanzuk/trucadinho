# README

-**PRÉ REQUISITOS**

ruby versão 2.4.1

rails versão 5.1.1

MySQL

-**CONFIGURANDO**
 

Para Rodar o sistema é necessário q se tenha o Image Magick Processor instalado,
 
para o correto funcionamento da gem paperclip, para instalar o Image Magick execute  o seguinte comando:

- sudo apt-get install imagemagick -y 

depois de instalar o Image Magick é necessário informar o paper clip onde o mesmo está intalado, rode o seguinte comando no terminal:

- which convert

ele deve retornar algo assim:

- /usr/local/bin/convert

então vá no projeto e ache o seguinte arquivo 

- config/environments/development.rb

e adicione o seguinte código, com base no que obteve de reposta após o comando which convert

- Paperclip.options[:command_path] = "/usr/local/bin/"


Agora precisamos levantar a base de dados.

Verifique o arquivo database.yml dentro de /config , veja se o mesmo está com usuário e senhas correspondentes aos configurados na sua base de dados MySQL
 
Se tudo estiver correto entre via termial no dirtório do projeto e execute o seguinte comando:

- rake db:create

Isso deve Criar a base de dados, logo em seguida execute o comando

- rake db:migrate

Isso deve criar as tabelas e inicializar dados na tabela Type

Pronto o sistema está rodando.

**CRIANDO USUÁRIO**

Eu não deixei link para cadastro de usuário pois acredio que o mesmo devesse ser protegido, porém para fim de teste do sistema proposto basta acessar a seguinte uri: 

- /users/new

Assim é possível criar um usuário e realizar os testes.
