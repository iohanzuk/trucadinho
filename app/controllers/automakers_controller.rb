class AutomakersController < ApplicationController
  before_action :set_automaker, only: [:show, :edit, :update, :destroy]

  # GET /automakers
  # GET /automakers.json
  def index
    @automakers = Automaker.all
  end

  # GET /automakers/1
  # GET /automakers/1.json
  def show
  end

  # GET /automakers/new
  def new
    @automaker = Automaker.new
  end

  # GET /automakers/1/edit
  def edit
  end

  # POST /automakers
  # POST /automakers.json
  def create
    @automaker = Automaker.new(automaker_params)

    respond_to do |format|
      if @automaker.save
        format.html { redirect_to @automaker, notice: 'Automaker was successfully created.' }
        format.json { render :show, status: :created, location: @automaker }
      else
        format.html { render :new }
        format.json { render json: @automaker.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /automakers/1
  # PATCH/PUT /automakers/1.json
  def update
    respond_to do |format|
      if @automaker.update(automaker_params)
        format.html { redirect_to @automaker, notice: 'Automaker was successfully updated.' }
        format.json { render :show, status: :ok, location: @automaker }
      else
        format.html { render :edit }
        format.json { render json: @automaker.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /automakers/1
  # DELETE /automakers/1.json
  def destroy
    @automaker.destroy
    respond_to do |format|
      format.html { redirect_to automakers_url, notice: 'Automaker was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_automaker
      @automaker = Automaker.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def automaker_params
      params.require(:automaker).permit(:name)
    end
end
