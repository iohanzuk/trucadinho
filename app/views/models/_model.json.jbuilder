json.extract! model, :id, :name, :automaker, :created_at, :updated_at
json.url model_url(model, format: :json)
