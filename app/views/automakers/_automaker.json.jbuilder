json.extract! automaker, :id, :name, :created_at, :updated_at
json.url automaker_url(automaker, format: :json)
