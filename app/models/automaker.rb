class Automaker < ApplicationRecord
  has_many :ads
  has_many :models
  validates_presence_of :name

end
