class Model < ApplicationRecord
  belongs_to :automaker
  validates_presence_of :automaker
end
