class Ad < ApplicationRecord
  belongs_to :type
  belongs_to :automaker
  belongs_to :model
  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/
  validates_presence_of :type, :automaker, :model
end
