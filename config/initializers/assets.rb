# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )

Rails.application.config.assets.precompile += %w( css/bootstrap.css )
Rails.application.config.assets.precompile += %w( font-awesome/css/font-awesome )
Rails.application.config.assets.precompile += %w( lineicons/style )
Rails.application.config.assets.precompile += %w( css/style )
Rails.application.config.assets.precompile += %w( css/style-responsive )

Rails.application.config.assets.precompile += %w( bootstrap.min.js )
Rails.application.config.assets.precompile += %w( jquery.js )
Rails.application.config.assets.precompile += %w( jquery-1.8.3.min )
Rails.application.config.assets.precompile += %w( jquery.scrollTo.min )
Rails.application.config.assets.precompile += %w( jquery.nicescroll )
Rails.application.config.assets.precompile += %w( jquery.sparkline )
Rails.application.config.assets.precompile += %w( common-scripts )