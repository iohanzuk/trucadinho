Rails.application.routes.draw do
  root to: 'ads#index'
  get 'users/login', to: 'login#new'
  post 'users/login', to: 'login#create'
  resources :users, only:  [:new, :create]
  get 'ads/machine', as: 'machine_ads', to:'ads#machine'
  get 'ads/truck', as: 'truck_ads', to:'ads#truck'
  get 'ads/update_models', as: 'update_models'
  get 'ads/:id/update_models', to:'ads#update_models'
  resources :ads
  resources :models
  resources :automakers

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
