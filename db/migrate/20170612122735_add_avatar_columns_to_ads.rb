class AddAvatarColumnsToAds < ActiveRecord::Migration[5.1]
  def up
    add_attachment :ads, :avatar
  end

  def down
    remove_attachment :ads, :avatar
  end
end
