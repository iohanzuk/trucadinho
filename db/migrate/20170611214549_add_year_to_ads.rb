class AddYearToAds < ActiveRecord::Migration[5.1]
  def change
    add_column :ads, :year, :int
  end
end
