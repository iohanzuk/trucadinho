class CreateAds < ActiveRecord::Migration[5.1]
  def change
    create_table :ads do |t|
      t.references :type
      t.references :automaker
      t.references :model
      t.decimal :price

      t.timestamps
    end
  end
end
